// TODO prevent overpayment
// TODO wire up return collateral button
// TODO Investments expandedRow with time-remaining and siezeCollateral 


import { Dharma, BigNumber } from "@dharmaprotocol/dharma.js";
import React, { Component } from "react";
import { InputGroup, FormGroup, FormControl, ControlLabel, ButtonToolbar, Button} from "react-bootstrap";

import BootstrapTable from "react-bootstrap-table-next";
import Title from "../Title/Title";
import Loading from "../Loading/Loading";
import Error from "../Error/Error";
import TransactionManager from "../TransactionManager/TransactionManager";
import AuthorizableAction from "../AuthorizableAction/AuthorizableAction";
import { seizedPanel } from "../tables.jsx";
import "./Debts.css";

const repaidFormatter = function repaidFormatter(cell, row) {
    if(row.isRepaid) {
        return (
            <span style={ {color: 'green'} }>
              { cell }
            </span>
        )
    } else {
      return (
          <span> { cell } </span>
      )
    }
}

const statusFormatter = function statusFormatter(cell, row) {
    if(row.status === "returned") {
        return (
            <span style={ {color: 'blue'} }>
              { cell }
            </span>
        )
    } else if(row.status === "seized" && row.status === "overdue") {
        return (
            <span style={ {color: 'red'} }>
              { cell }
            </span>
        )
    } else if (row.status === "repaid"){
        return (
            <span style={ {color: 'green'} }>
              { cell }
            </span>
        )
    }
    return ( <span>{cell}</span>)
}

const columns = [
    {
        dataField: "status",
        text: "Loan Status",
        formatter: statusFormatter,
    },
    {
        dataField: "principal",
        text: "Principal",
    },
    {
        dataField: "interestRate",
        text: "Interest Rate",
    },
    {
        dataField: "term",
        text: "Term Length",
    },
    {
        dataField: "collateral",
        text: "Collateral",
    },
    {
        dataField: "repaidAmount",
        text: "Repaid",
        formatter: repaidFormatter,
    },
    {
        dataField: "totalExpectedRepaymentAmount",
        text: "Total Expected Repayment",
    },
];


class RepaymentPanel extends Component {
    constructor(props) {
        super(props);
        this.getData = this.getData.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setHasSufficientAllowance = this.setHasSufficientAllowance.bind(this);
        this.authorizePrincipalTransfer = this.authorizePrincipalTransfer.bind(this);
        this.returnCollateral = this.returnCollateral.bind(this);
        this.state = {
            repaymentDates: [],
            canReturnCollateral: null,
            paymentValue: null,
            hasSufficientAllowance: null,
            numDecimals: null,
            txHash: null,
            debtId: null,
            error: null
        };
    }

    async getData(debt) {
        const { dharma } = this.props;
        const { Debt } = Dharma.Types;
        const {
            servicing,
            adapters: { collateralizedSimpleInterestLoan: loan },
        } = dharma;
        const entry = await servicing.getDebtRegistryEntry(debt.id);
        const dateArray = await loan.getRepaymentSchedule(entry);
        const numDecimals = await dharma.token.getNumDecimals(debt.principalTokenSymbol);
        const canReturnCollateral = await loan.canReturnCollateral(debt.id)
        const myLoan = await Debt.fetch(dharma, debt.id)
        const debtOutstanding = await myLoan.getOutstandingAmount();

        this.setState({
            debtId: debt.id,
            paymentValue: debtOutstanding,
            numDecimals: numDecimals,
            repaymentDates: dateArray,
            canReturnCollateral
        });
    }

    dblDigits = (num) => {if(num < 10) { return ("0" + num) } else return num};
    handleChange = (e) => {
        this.setState({ paymentValue: e.target.value })

        this.setHasSufficientAllowance(e.target.value);
    }

    async returnCollateral(e){
        const { dharma } = this.props;
        const { adapters: { collateralizedSimpleInterestLoan: loan } } = dharma;
        const result = await loan.returnCollateralAsync(this.state.debtId);
        this.forceUpdate();
    }

    async getDecimals () {

    }
    async handleSubmit (e) {
        // call submit on dharma
        const {debt, dharma} = this.props;
        const { contracts, servicing } = dharma;
        const tokenAddress = await contracts.getTokenAddressBySymbolAsync(debt.principalTokenSymbol);
        const perUnit = (new BigNumber(10)).pow(this.state.numDecimals);
        console.log( "perUnit: ", perUnit )
        const paymentAmt = (new BigNumber(this.state.paymentValue)).times(perUnit);
        console.log("paymentAmt: ", paymentAmt.toString())
        const result = await servicing.makeRepayment(debt.id, paymentAmt, tokenAddress);
        console.log(result);
        if(this.props.onSubmit) {this.props.onSubmit();}
    }

    async setHasSufficientAllowance(value) {
        const {
            debt: {
                principalTokenSymbol,
                principalAmount
            },
            dharma
        } = this.props;
        const { Token } = Dharma.Types;
        const currentAccount = await dharma.blockchain.getCurrentAccount();
        const tokenData = await Token.getDataForSymbol(dharma, principalTokenSymbol, currentAccount);
        const hasSufficientAllowance = tokenData.hasUnlimitedAllowance || tokenData.allowance >= principalAmount;
        this.setState({
            hasSufficientAllowance,
        })
    }

    async authorizePrincipalTransfer () {
        const { debt: {principalTokenSymbol}, dharma } = this.props;
        const { Token } = Dharma.Types;
        console.log('principalTokenSymbol:', principalTokenSymbol)
        const currentAccount = await dharma.blockchain.getCurrentAccount();
        console.log('account:', currentAccount)
        const txHash = await Token.makeAllowanceUnlimitedIfNecessary(
            dharma, principalTokenSymbol, currentAccount
        );
        console.log('txHash:', txHash)
        this.setState({txHash})
    }

    componentDidMount() {
        const {debt} = this.props;
        this.getData(debt);
        this.setHasSufficientAllowance();
        this.counter = setInterval(()=>{this.forceUpdate();}, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.counter);
    }

    render() {
        const { debt, dharma } = this.props;
        const { txHash,error } = this.state;
        const today = new Date();
        const monthNames = ["January", "February", "March", "April",
                            "May", "June", "July", "August",
                            "September", "October", "November", "December"];
        const nextDueDate = new Date(this.state.repaymentDates[0] * 1000);
        let secLeft = Math.floor((nextDueDate - today) / 1000);
        let minLeft = Math.floor(secLeft/60);
        let hoursLeft = Math.floor(minLeft/60);
        let daysLeft = Math.floor(hoursLeft/24);
        hoursLeft = this.dblDigits(hoursLeft - (daysLeft*24));
        minLeft = this.dblDigits(minLeft - (daysLeft*24*60) - (hoursLeft*60));
        secLeft = this.dblDigits(secLeft - (daysLeft*24*60*60) - (hoursLeft * 60 * 60) - (minLeft * 60));
        let amOrPm = (nextDueDate.getHours() > 12) ? "PM" : "AM";

        return this.state.repaymentDates.length === 0 ? (
            <Loading />
        ) : (
            <div className="expanded-debt">
            { error && (<Error>{error}</Error>)}
            { txHash && (
              <TransactionManager
                className="repayment-transaction"
                key={txHash}
                txHash={txHash}
                dharma={dharma}
                description="Authorize Token Transfer"
                onSuccess={this.setHasSufficientAllowance} />
            )}
            <div className="debt-time-remaining">
              <div className="clock">
                <div className="debt-due-date">
                  <div className="debt-due-date-label">Next Due Date:</div>
                  <div className="debt-due-date-value">
                    {monthNames[nextDueDate.getMonth()]}
                    {" "}
                    {nextDueDate.getDate()}
                    {" "}
                    {nextDueDate.getFullYear()}
                    {" "}
                    at {nextDueDate.getHours() % 12}:{nextDueDate.getMinutes()} {amOrPm}
                  </div>
                </div>
                <div className="days-label">Days</div>
                <div className="hours-label">Hrs</div>
                <div className="mins-label">Mins</div>
                <div className="secs-label">Secs</div>
                <div className="days-remaining">{daysLeft}</div>
                <div className="hours-remaining">{hoursLeft}</div>
                <div className="mins-remaining">{minLeft}</div>
                <div className="secs-remaining">{secLeft}</div>
                </div>
              </div>

              <div className="debt-make-payment">
                <div className="make-payment-contents">
                  <div className="debt-payment-controls">
                    <form>
                      <FormGroup >
                        <ControlLabel>Payment Amount</ControlLabel>
                        <InputGroup>
                          <FormControl
                              type="number"
                              value={this.state.paymentValue}
                              placeholder={"Enter an amount"}
                              onChange={this.handleChange} >
                          </FormControl>
                          <InputGroup.Addon>{debt.principalTokenSymbol}</InputGroup.Addon>
                        </InputGroup>
                      </FormGroup>
                    </form>
                    <ButtonToolbar>
                      <AuthorizableAction
                        className="Action inline"
                        canTakeAction={this.state.hasSufficientAllowance}
                        canAuthorize={
                            this.state.hasSufficientAllowance !== null && !this.state.hasSufficientAllowance
                        }
                        onAction={this.handleSubmit}
                        onAuthorize={this.authorizePrincipalTransfer}>
                        Submit Payment
                      </AuthorizableAction>
                      <Button
                          onClick={this.returnCollateral}
                          disabled={!this.state.canReturnCollateral}>Return Collateral</Button>
                    </ButtonToolbar>
                  </div>
                </div>
              </div>
            </div>
        );
    }
}

class Debts extends Component {
    constructor(props) {
        super(props);
        this.state = { debts: null };
        this.getData = this.getData.bind(this);
        this.onSubmitPayment = this.onSubmitPayment.bind(this);
        this.isSeized = this.isSeized.bind(this);
        this.isReturned = this.isReturned.bind(this);
        this.isOverdue = this.isOverdue.bind(this);
    }

    isReturned(dharma, debt) {
        const { adapters: { collateralizedSimpleInterestLoan: loan } } = dharma;
        return new Promise((res, rej) => {return res(loan.isCollateralReturned(debt.id))});
    }

    isSeized(dharma, debt) {
        const { adapters: { collateralizedSimpleInterestLoan: loan } } = dharma;
        return new Promise((res, rej) => {return res(loan.isCollateralSeized(debt.id))});
    }

    async isOverdue(dharma, debt) {
        const {
            servicing,
            adapters: { collateralizedSimpleInterestLoan: loan },
        } = dharma;
        const entry = await servicing.getDebtRegistryEntry(debt.id);
        const dateArray = await loan.getRepaymentSchedule(entry);
        const today = new Date();
        const tillDue = 1000*dateArray[0] - today;
        console.log("tillDue: ", tillDue)
        return ( tillDue < 0 );
    }


    async componentDidMount() {
        const { dharma } = this.props;

        const { adapters: { collateralizedSimpleInterestLoan: loan } } = dharma;

        const { Debts } = Dharma.Types;

        const debtor = await dharma.blockchain.getCurrentAccount();

        const debts = await Debts.getExpandedData(dharma, debtor);

        console.log("debts:",debts);


        this.setState({ debts });

        const addReturnedSeized = async () => {
            const all = Promise.all(
                debts.map(async d => {
                    let dd = Object.assign({}, d);
                    const returned = await this.isReturned(dharma, d);
                    const seized = await this.isSeized(dharma, d);
                    dd.isReturned = returned;
                    dd.isSeized = seized;
                    dd.isRepaid = ( d.repaidAmount >= d.totalExpectedRepaymentAmount );
                    dd.isOverdue = await this.isOverdue(dharma, d);

                    return dd;
                })
            )
            return await all;
        };

        const xdebts = await addReturnedSeized();

        this.setState({debts: xdebts});
        // add in collateral seized red

        //const debts = await loan.canSeizeCollateral(debt.id)

    }

    getData() {
        const { debts } = this.state;

        // const valFromCurrencyString = /(\d\.?\d*)\s\w+$/; // finds number such as 1 in "1 eth" or 10.388 in "10.388 WETH"

        if (!debts) {
            return null;
        }

        const debtStatus = (d) => {
            if(d.isSeized == null) {return "loading..."}
            if(d.isSeized) {
                return "seized";
            }
            else if (d.isReturned) {
                return "returned";
            }
            else if (d.isRepaid) {
                return "repaid";
            }
            else if (d.isOverdue){
                return "overdue"
            } else { return "active" }

        }
        console.log(debts)

        return debts.map((debt) => {
            return {
                ...debt,
                isRepaid: (debt.repaidAmount >= debt.totalExpectedRepaymentAmount),
                status: debtStatus(debt),
                repaidValue: `${debt.repaidAmount}`,
                expectedValue: `${debt.totalExpectedRepaymentAmount}`,
                principal: `${debt.principalAmount} ${debt.principalTokenSymbol}`,
                collateral: `${debt.collateralAmount} ${debt.collateralTokenSymbol}`,
                term: `${debt.termDuration} ${debt.termUnit}`,
                repaidAmount: `${debt.repaidAmount} ${debt.principalTokenSymbol}`,
                totalExpectedRepaymentAmount: `${debt.totalExpectedRepaymentAmount} ${
                    debt.principalTokenSymbol
                }`,
            };
        });
    }

    onSubmitPayment() {
        this.forceUpdate();
    }

    render() {
        const data = this.getData();
        const expandRow = {
            onlyOneExpanding: true,
            renderer: (row) => {
                return <RepaymentPanel
                           dharma={this.props.dharma}
                           debt={row}
                           onSubmit={this.onSubmitPayment} />;
            },
        };
        if (!data) {
            return <Loading />;
        }

        return (
            <div className="Debts">
                <Title>Your Debts</Title>
                {(() => {
                    if (data && data.length === 0) {
                        return <div className="empty-table">You have no outstanding loans.</div>;
                    } else if (data) {
                        return (
                            <BootstrapTable
                                keyField="id"
                                columns={columns}
                                data={data}
                                expandRow={expandRow}
                            />
                        );
                    } else {
                        return <Loading />;
                    }
                })()}
            </div>
        );
    }
}

export default Debts;

