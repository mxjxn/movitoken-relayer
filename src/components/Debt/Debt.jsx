import { Dharma } from "@dharmaprotocol/dharma.js";
import React, { Component } from "react";

//import TransactionManager from "../TransactionManager/TransactionManager";
//import Loading from "../Loading/Loading";

//import { LinkContainer } from "react-router-bootstrap";

//import { Breadcrumb, Panel } from "react-bootstrap";

class Debt extends Component {
    constructor(props) {
        super(props);

        // this.state = {}
    }
    async componentDidMount() {
        // get debt status from Dharma
        const { dharma } = this.props;
        const { Debt } = Dharma.Types;
        const debtor = await dharma.blockchain.getCurrentAccount();

        const debts = await Debt.getExpandedData(dharma, debtor);
    }
}
