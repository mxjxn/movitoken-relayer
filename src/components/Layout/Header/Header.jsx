// External libraries
import React, { Component } from "react";
import { Image, Navbar, Nav, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

// Styling
import "./Header.css";
class Header extends Component {
    render() {
        return (
            <Navbar className="Header">
                <Navbar.Header>
                    <Navbar.Brand>
                        <Image src="images/MoviLine_BETA_logo_web.png" />
                    </Navbar.Brand>
                </Navbar.Header>

                <Nav>
                    <LinkContainer to="/" exact={true}>
                        <NavItem>Browse</NavItem>
                    </LinkContainer>

                    <LinkContainer to="/create">
                        <NavItem>Create</NavItem>
                    </LinkContainer>

                    <LinkContainer to="/tokens">
                        <NavItem>Tokens</NavItem>
                    </LinkContainer>

                    <LinkContainer to="/investments">
                        <NavItem>Investments</NavItem>
                    </LinkContainer>

                    <LinkContainer to="/debts">
                        <NavItem>Debts</NavItem>
                    </LinkContainer>
                </Nav>
            </Navbar>
        );
    }
}

export default Header;
