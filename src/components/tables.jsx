
import { Dharma, BigNumber } from "@dharmaprotocol/dharma.js";
import React, { Component } from "react";
// import { InputGroup, FormGroup, FormControl, ControlLabel, ButtonToolbar, Button} from "react-bootstrap";
// import TransactionManager from "../TransactionManager/TransactionManager";
// import AuthorizableAction from "../AuthorizableAction/AuthorizableAction";

import "./tables.css";

// dblDigits = (num) => {if(num < 10) { return ("0" + num) } else return num};


export function seizedPanel () {
    return (
        // collateral has been seized
        <div className="seized-collateral">
          The collateral has been seized for this loan.
        </div>
    )
}

