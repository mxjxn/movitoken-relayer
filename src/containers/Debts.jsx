import React, { Component } from "react";

import DharmaConsumer from "../contexts/Dharma/DharmaConsumer";
import Loans from "../components/Debts/Debts";

class DebtsContainer extends Component {
    constructor(props) {
        super(props);

        this.redirect = this.redirect.bind(this);
        this.parseQueryParams = this.parseQueryParams.bind(this);
    }

    redirect(location) {
        this.props.history.push(location);
    }

    parseQueryParams() {
        const search = this.props.location.search;
        const params = new URLSearchParams(search);
        const rowToHighlight = params.get("highlightRow");

        if (rowToHighlight) {
            return parseInt(rowToHighlight, 10);
        } else {
            return null;
        }
    }
    render() {
        return (
            <DharmaConsumer>
                {(dharmaProps) => <Loans dharma={dharmaProps.dharma} redirect={this.redirect} />}
            </DharmaConsumer>
        );
    }
}

export default DebtsContainer;
